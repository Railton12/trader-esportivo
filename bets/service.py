import requests
from decouple import config

def get_api():
    url = "https://v3.football.api-sports.io/odds?season=2019&bet=1&bookmaker=6&fixture=157140&league=39"
    headers = {
        'x-apisports-key': config('headers')
    }

    response = requests.request("GET", url, headers=headers)

    droplets = response.json()
    droplets_list = []
    for i in range(len(droplets['response'])):
        droplets_list.append(droplets['response'][i])
    return droplets_list