from django.views.generic import TemplateView
from .service import get_api

class IndexView(TemplateView):
    template_name = "index.html"
    def get_context_data(self, *args, **kwargs):
        context = {
            'droplets': get_api
        }
        return context